#use wml::debian::translation-check translation="c293bd2d1bd25d64d75b226599ea7fc6579da6ad" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8020">CVE-2020-8020</a>

<p>Une vulnérabilité de neutralisation incorrecte d’entrée lors de la génération
de page web dans open-build-service permet à des attaquants distants de stocker
du code JavaScript arbitraire pour un attaque XSS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8021">CVE-2020-8021</a>

<p>Une vulnérabilité de contrôle d’accès incorrect dans open-build-service
permet à des attaquants distants de lire des fichiers d’un paquet OBS où
sourceaccess/access est désactivé.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 2.7.1-10+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets open-build-service.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de open-build-service, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/open-build-service">https://security-tracker.debian.org/tracker/open-build-service</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2545.data"
# $Id: $
