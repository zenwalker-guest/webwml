#use wml::debian::translation-check translation="e41f5efa353b2bdd34609a011c02c9132873e041" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>To sårbarheder er fundet i implementeringen af WPA-protokollen, som findes i 
wpa_supplication (station) og hostapd (accesspoint).</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13377">CVE-2019-13377</a>

    <p>Et timingbaseret sidekanalsangreb mod WPA3's Dragonfly-håndtryk, når der 
    anvendes Brainpool-kurver, kunne udnyttes af en angriber til at få fat i 
    adgangskoden.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16275">CVE-2019-16275</a>

    <p>Utilstrækkelig validering af kildeadresse for nogle modtagne 
    Management-frames i hostapd, kunne føre til et lammelsesangreb for stationer 
    forbundet med et accesspoint.  En angriber indenfor radioafstand af 
    accesspoint'et, kunne indsprøjte en særligt fremstillet uautentificeret 
    IEEE 802.11-frame til accesspoint'et, og dermed forårsage at forbindelsen 
    til forbundne stationer blev afbrudt, og krævede en ny forbindelse til 
    netværket.</p></li>

</ul>

<p>I den stabile distribution (buster), er disse problemer rettet i
version 2:2.7+git20190128+0c1e29f-6+deb10u1.</p>

<p>Vi anbefaler at du opgraderer dine wpa-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende wpa, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/wpa">\
https://security-tracker.debian.org/tracker/wpa</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4538.data"
