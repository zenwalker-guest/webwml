#use wml::debian::template title="Nuestra filosofía: por qué lo hacemos y cómo lo hacemos"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="8d625100deb403dba223e3cecbac44a283fe02ff"

# translators: some text is taken from /intro/about.wml

<ul class="toc">
<li><a href="#what">¿QUÉ es Debian?</a>
<li><a href="#free">¿Todo esto es gratis?</a>
<li><a href="#how">¿Cómo hace la comunidad para funcionar como un proyecto?</a>
<li><a href="#history">¿Cómo empezó todo?</a>
</ul>

<h2><a name="what">¿QUÉ es Debian?</a></h2>

<p>El <a href="$(HOME)/">proyecto Debian</a> es una asociación de
personas que han hecho causa común para crear un sistema operativo
<a href="free">libre</a>. Este sistema operativo que hemos creado se llama
<strong>Debian</strong>.</p>

<p>Un sistema operativo es un conjunto de programas y utilidades básicas que hacen
que su computadora funcione.
El centro de un sistema operativo es el núcleo (N. del T.: kernel).
El núcleo es el programa más importante en la computadora, realiza todo el trabajo
básico y le permite ejecutar otros programas.</p>

<p>Los sistemas Debian actualmente usan el núcleo de
<a href="https://www.kernel.org/">Linux</a> o el de <a href="https://www.freebsd.org/">FreeBSD</a>.
Linux es una pieza de software creada en un principio por
<a href="https://en.wikipedia.org/wiki/Linus_Torvalds">Linus Torvalds</a>
y desarrollada por miles de programadores alrededor del mundo.
FreeBSD es un sistema operativo que incluye un núcleo y otro software.</p>

<p>Sin embargo, se está trabajando para ofrecer Debian con otros núcleos,
en especial con
<a href="https://www.gnu.org/software/hurd/hurd.html">el Hurd</a>.
El Hurd es una colección de servidores que se ejecutan sobre un micronúcleo (como
Mach) para implementar las distintas funcionalidades. El Hurd es software libre producido por el
<a href="https://www.gnu.org/">proyecto GNU</a>.</p>

<p>Una gran parte de las herramientas básicas que completan el sistema operativo provienen
del <a href="https://www.gnu.org/">proyecto GNU</a>, de ahí los nombres:
GNU/Linux, GNU/kFreeBSD y GNU/Hurd.
Estas herramientas también son libres.</p>

<p>Desde luego, lo que la gente quiere es el software de aplicación: herramientas
que le ayuden a realizar lo que necesita hacer, desde editar documentos o
llevar un negocio hasta divertirse con juegos o escribir más software. Debian viene
con más de <packages_in_stable> <a href="$(DISTRIB)/packages">paquetes</a> (software
precompilado y empaquetado en un formato amigable para una instalación sencilla en su
máquina), un gestor de paquetes (APT) y otras utilidades que hacen posible
gestionar miles de paquetes en miles de ordenadores de manera tan fácil como
instalar una sola aplicación. Todo ello <a href="free">libre</a>.
</p>

<p>Es un poco como una torre. En la base está el núcleo.
Encima se encuentran todas las herramientas básicas.
Después está el software que usted ejecuta en su computadora.
En la cima de la torre se encuentra Debian, organizando y encajando las piezas
cuidadosamente para que todo el sistema trabaje conjuntamente.</p>

<h2><a href="free" name="free">¿Todo esto es gratis?</a></h2>

<p>Cuando usamos el término «libre», nos referimos a la <strong>libertad</strong>
del software, no a que tenga que ser gratuito (N. del T.: en inglés, «free» es sinónimo de «gratuito» y de «libre»). Puede leer más sobre
<a href="free">lo que consideramos software libre</a> y
sobre <a href="https://www.gnu.org/philosophy/free-sw">lo que dice la Fundación para el Software
Libre</a> a este respecto.</p>

<p>Usted puede preguntarse: ¿por qué dedicaría la gente horas de su tiempo a escribir
software y a empaquetarlo cuidadosamente, para luego <EM>regalarlo</EM>?
Las respuestas son tan variadas como la gente que contribuye.
A algunas personas les gusta ayudar a otras.
Muchas escriben programas para aprender más acerca de los computadores.
Más y más personas están buscando maneras de evitar los inflados precios del
software.
Un grupo creciente contribuye como agradecimiento por todo el excelente software libre y gratuito que ha
recibido de otros.
En las instituciones académicas muchos crean software libre para ayudar a la expansión de
los resultados de sus investigaciones.
Las empresas ayudan a mantener el software libre para poder influir en su desarrollo,
¡no hay manera más rápida de obtener una nueva funcionalidad que implementarla uno mismo!
Desde luego, muchos de nosotros simplemente lo encontramos divertido.</p>

<p>Debian está tan comprometido con el software libre que pensamos que sería útil
formalizar ese compromiso en un documento escrito. Por ello nació nuestro
<a href="$(HOME)/social_contract">Contrato social</a>.

<p>Aunque Debian cree en el software libre, existen casos en los que la gente quiere o necesita
disponer de software que no es libre en sus máquinas. Siempre que sea posible, Debian respaldará esto.
Hay un número creciente de paquetes que tienen como única misión instalar software que no es libre
en un sistema Debian.</p>

<h2><a name="how">¿Cómo hace la comunidad para funcionar como un proyecto?</a></h2>

<p>Debian es producido por cerca de un millar de desarrolladores
activos distribuidos
<a href="$(DEVEL)/developers.loc">por todo el mundo</a> que trabajan de forma voluntaria
en su tiempo libre.
Pocos de ellos se conocen en persona.
La comunicación se realiza, principalmente, a través de correo electrónico (listas de correo en
lists.debian.org) y de IRC (canal #debian en irc.debian.org).
</p>

<p>El proyecto Debian tiene una <a href="organization">estructura organizada</a>
cuidadosamente. Si desea más información sobre cómo es Debian por dentro,
siéntase libre de navegar por el <a href="$(DEVEL)/">rincón del desarrollador</a>.</p>

<p>
Los principales documentos que explican el funcionamiento de la comunidad son los siguientes:
<ul>
<li><a href="$(DEVEL)/constitution">Constitución de Debian</a></li>
<li><a href="../social_contract">Contrato social y Directrices de software libre</a></li>
<li><a href="diversity">Declaración de diversidad</a></li>
<li><a href="../code_of_conduct">Código de conducta</a></li>
<li><a href="../doc/developers-reference/">Referencia del desarrollador</a></li>
<li><a href="../doc/debian-policy/">Manual de normas</a></li>
</ul>

<h2><a name="history">¿Cómo empezó todo?</a></h2>

<p>Debian comenzó en agosto de 1993 gracias a Ian Murdock como una nueva distribución
que se realizaría de forma abierta, en la línea del espíritu de Linux y GNU. Debian estaba pensada
para ser creada de forma cuidadosa y concienzuda, y ser mantenida y
soportada con el mismo cuidado. Comenzó como un grupo de pocos y fuertemente unidos
hackers de software libre y creció gradualmente hasta convertirse en una comunidad grande y bien
organizada de desarrolladores y usuarios. Vea
<a href="$(DOC)/manuals/project-history/">la historia detallada</a>.</p>

<p>Ya que mucha gente lo ha preguntado, Debian se pronuncia /&#712;de.bi.&#601;n/.
Viene de los nombres del creador de Debian, Ian Murdock, y de su esposa,
Debra.</p>
