#use wml::debian::template title="Redenen om voor Debian te kiezen" MAINPAGE="true"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="7141cf5f25b03b1aa3c02c1afe2a5ca8cab2f672"

<p>Gebruikers hebben verschillende redenen om te kiezen voor Debian als hun besturingssysteem.</p>

<h1>Belangrijkste redenen</h1>

# try to list reasons for end users first

<dl>
  <dt><strong>Debian is vrije software.</strong></dt>
  <dd>
    Debian is gemaakt van vrije en openbronsoftware en zal altijd 100%
    <a href="free">vrij</a> zijn. Vrij om door iedereen gebruikt, aangepast en verspreid te worden. Dit is de belangrijkste belofte die we aan <a href="../users">onze gebruikers</a> doen. Debian is ook kosteloos.
  </dd>
</dl>

<dl>
  <dt><strong>Debian is een op Linux gebaseerd besturingssysteem dat stabiel en veilig is.</strong></dt>
  <dd>
    Debian is een besturingssysteem voor een brede waaier apparaten,
    waaronder laptops, desktops en servers. Gebruikers waarderen zijn
    stabiliteit en betrouwbaarheid sinds 1993. Voor elk pakket voorzien
    we een gefundeerde standaardconfiguratie.
    De ontwikkelaars van Debian voorzien in veiligheidsupdates voor alle
    pakketten, gedurende hun volledige levensduur en voor zover dit enigszins
    mogelijk is.
  </dd>
</dl>

<dl>
  <dt><strong>Debian heeft uitgebreide ondersteuning voor hardware.</strong></dt>
  <dd>
    De Linux kernel ondersteunt reeds meeste hardware.
    Gepatenteerde stuurprogramma's voor hardware zijn beschikbaar als
    de vrije software niet toereikend is.
  </dd>
</dl>

<dl>
  <dt><strong>Debian voorziet in vlotte upgrades.</strong></dt>
  <dd>
    Debian staat bekend voor zijn gemakkelijke en vlotte upgrades binnen een
   releasecyclus maar ook voor die naar de volgende hoofdrelease.
  </dd>
</dl>

<dl>
  <dt><strong>Debian is de kiem en de basis van veel andere distributies.</strong></dt>
  <dd>
    Veel van de populairste Linux-distributies, zoals Ubuntu, Knoppix, PureOS,
    SteamOS en Tails, kiezen Debian as basis voor hun software.
    Debian stelt alle hulpmiddelen ter beschikking waarmee iedereen de
    softwarepakketten uit het archief van Debian, kan aanvullen met zijn eigen
    pakketten, welke tegemoet komen aan de eigen noden.
  </dd>
</dl>

<dl>
  <dt><strong>Het Debian project is een samenwerkingsverband.</strong></dt>
  <dd>
    Debian is niet enkel een Linux-besturingssysteem. De software wordt
    gezamenlijk gemaakt door honderden vrijwilligers van over de hele
    wereld. U kunt ook deel uitmaken van de Debian-gemeenschap, zelfs
    wanneer u geen programmeur of systeembeheerder bent. Debian wordt
    door de gemeenschap in consensus gestuurd en heeft een
    <a href="../devel/constitution">democratische beleidsstructuur</a>.
    Vermits alle ontwikkelaars van Debian dezelfde rechten hebben, kan het
    niet door één bedrijf gecontroleerd worden. We hebben in meer dan 60
    landen ontwikkelaars en bieden in meer dan 80 talen ondersteuning voor
    ons Debian installatiesysteem.
  </dd>
</dl>

<dl>
  <dt><strong>Debian heeft verschillende installatieopties.</strong></dt>
  <dd>
    Eindgebruikers gebruiken onze
    <a href="https://cdimage.debian.org/images/release/current-live/amd64/iso-hybrid/">Live CD</a>
    met het makkelijk te gebruiken Calamares installatiesysteem dat maar
    heel weinig invoer en voorkennis vereist. Meer ervaren
    gebruikers kunnen gebruik maken van ons uniek en veelzijdig
    installatiesysteem en experten kunnen dit installatiesysteem haarfijn
    afstellen of zelfs een hulpmiddel gebruiken om een geautomatiseerde
    installatie over het netwerk uit te voeren.
  </dd>
</dl>

<br>

<h1>Bedrijfsomgeving</h1>

<p>
  Indien u Debian in een professionele omgeving nodig heeft, geniet u van
  de volgende extra voordelen:
</p>

<dl>
  <dt><strong>Debian is betrouwbaar.</strong></dt>
  <dd>
    Debian toont dagelijks zijn betrouwbaarheid aan in duizenden
    praktijkscenario's, gaande van één enkele laptop tot
    deeltjesversnellers, effectenbeurzen en de auto-industrie. Het is ook
    populair in de academische wereld en in de wetenschappelijke en de
    publieke sector.
  </dd>
</dl>

<dl>
  <dt><strong>Debian beschikt over vele experten.</strong></dt>
  <dd>
    De onderhouders van onze pakketten zorgen niet enkel voor de verpakking
    binnen Debian en het integreren van nieuwe bovenstroomse versies. Vaak
    zijn ze ook experten in de software die deze toeleveraars ontwikkelen
    en dragen ze ook rechtstreeks bij tot deze ontwikkeling. Soms maken ze
    ook deel uit van dat bovenstroomse ontwikkelingsteam.
  </dd>
</dl>

<dl>
  <dt><strong>Debian is veilig.</strong></dt>
  <dd>
    Debian biedt beveiligingsondersteuning voor zijn stabiele releases.
    Veel andere distributies en beveiligingsonderzoekers steunen
    op het beveiligingsvolgsysteem van Debian.
  </dd>
</dl>

<dl>
  <dt><strong>Langetermijnondersteuning.</strong></dt>
  <dd>
    Er bestaat kosteloze <a href="https://wiki.debian.org/LTS">
    Langetermijnondersteuning</a> (Long Term Support - LTS).
    Daardoor geniet u uitgebreide ondersteuning voor de stabiele release
    gedurende 5 jaar en langer. Daarnaast is er ook het initiatief voor een
    <a href="https://wiki.debian.org/LTS/Extended">Verlengde LTS</a>,
    waardoor een beperkt aantal pakketten langer dan 5 jaar ondersteuning
    krijgt.
  </dd>
</dl>

<dl>
  <dt><strong>Cloud-images.</strong></dt>
  <dd>
    Er zijn officiële cloud-images beschikbaar voor alle belangrijke
    cloudplatformen. We stellen ook de hulpmiddelen en de configuratie
    ter beschikking waarmee u uw eigen aangepast cloud-image kunt bouwen.
    U kunt Debian ook gebruiken in een virtuele machine op de desktop
    of in een container.
  </dd>
</dl>

<br>

<h1>Ontwikkelaars</h1>
<p>Debian wordt veel gebruikt door allerlei soorten software- en
   hardwareontwikkelaars.</p>

<dl>
  <dt><strong>Publiek beschikbaar bugvolgsysteem.</strong></dt>
  <dd>
    Ons Debian <a href="../Bugs">Bug tracking system</a> (BTS -
    bugvolgsysteem) is via een webbrowser publiek beschikbaar voor iedereen.
    Wij verbergen de bugs in onze software niet en u kunt gemakkelijk
    nieuwe bugrapporten indienen.
  </dd>
</dl>

<dl>
  <dt><strong>IoT en ingebedde apparatuur.</strong></dt>
  <dd>
    We bieden ondersteuning voor een ruime waaier apparatuur, zoals de
    Raspberry Pi, varianten van QNAP, mobiele apparaten, thuisrouters
    en veel Single Board Computers (SBC).
  </dd>
</dl>

<dl>
  <dt><strong>Verschillende hardware-architecturen.</strong></dt>
  <dd>
    Er bestaat ondersteuning voor een <a href="../ports">lange lijst</a>
    CPU-architecturen, waaronder amd64, i386, verschillende versies van ARM
    en MIPS, POWER7, POWER8, IBM System z, RISC-V. Debian is ook beschikbaar
    voor oudere en specifieke niche-architecturen.
  </dd>
</dl>

<dl>
  <dt><strong>Een enorm aantal softwarepakketten zijn beschikbaar.</strong></dt>
  <dd>
    Debian stelt het grootste aantal geïnstalleerde pakketten
    (momenteel <packages_in_stable>) ter beschikking. Onze pakketten worden
    gemaakt in de deb-indeling, welke bekend staat voor haar hoge kwaliteit.
  </dd>
</dl>

<dl>
  <dt><strong>Keuze uit verschillende releases.</strong></dt>
  <dd>
    Naast onze stabiele release, krijgt u de recentste versies van software
    wanneer u de 'testing' of de 'unstable' release gebruikt.
  </dd>
</dl>

<dl>
  <dt><strong>Hoge kwaliteit mede dankzij ontwikkelaarsgereedschap en beleidsrichtlijnen.</strong></dt>
  <dd>
    Verschillende hulpmiddelen voor ontwikkelaars helpen bij het op een hoog
    niveau houden van de kwaliteit en onze
    <a href="../doc/debian-policy/">beleidsrichtlijnen</a> bepalen de
    technische vereisten waaraan elk pakket moet voldoen om opgenomen te
    worden in de distributie. De doorlopende integratie van nieuwe pakketten
    verloopt onder controle van de autopkgtest-software, piuparts is het
    gereedschap waarmee we de installatie, opwaardering en verwijdering
    van een pakket testen en lintian is een comprehensief hulpmiddel
    voor de controle van Debian pakketten.
  </dd>
</dl>

<br>

<h1>Wat onze gebruikers zeggen</h1>

<ul style="line-height: 3em">
  <li>
    <q><strong>
      Voor mij is dit het perfecte niveau van gebruiksgemak en stabiliteit.
      In de loop van de jaren heb ik verschillende andere distributies
      gebruikt, maar Debian is de enige die gewoon werkt.
     </strong></q>
  </li>

  <li>
    <q><strong>
      Uiterst stabiel. Immens veel pakketten. Excellente gemeenschap.
    </strong></q>
  </li>

  <li>
    <q><strong>
      Voor mij staat Debian symbool voor stabiliteit en gebruiksgemak.
    </strong></q>
  </li>
</ul>
