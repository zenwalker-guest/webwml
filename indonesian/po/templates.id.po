# Izharul Haq <atoz@debian-id.org>, 2010.
msgid ""
msgstr ""
"Project-Id-Version: templates.po\n"
"PO-Revision-Date: 2010-11-18 19:08+0700\n"
"Last-Translator: Izharul Haq <atoz@debian-id.org>\n"
"Language-Team: Indonesian <debian-l10n@debian-id.org>\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/search.xml.in:7
#, fuzzy
msgid "Debian website"
msgstr "Proyek Debian"

#: ../../english/search.xml.in:9
msgid "Search the Debian website."
msgstr ""

#: ../../english/template/debian/basic.wml:19
#: ../../english/template/debian/navbar.wml:11
#, fuzzy
msgid "Debian"
msgstr "Sokong Debian"

#: ../../english/template/debian/basic.wml:48
msgid "Debian website search"
msgstr ""

#: ../../english/template/debian/common_translation.wml:4
msgid "Yes"
msgstr "Ya"

#: ../../english/template/debian/common_translation.wml:7
msgid "No"
msgstr "Tidak"

#: ../../english/template/debian/common_translation.wml:10
msgid "Debian Project"
msgstr "Proyek Debian"

#: ../../english/template/debian/common_translation.wml:13
#, fuzzy
msgid ""
"Debian is an operating system and a distribution of Free Software. It is "
"maintained and updated through the work of many users who volunteer their "
"time and effort."
msgstr ""
"Debian GNU/Linux adalah distribusi sistem operasi GNU/Linux yang free. "
"Dimaintain dan diupdate oleh para pengguna yang mendedikasikan waktu dan "
"usahanya secara sukarela."

#: ../../english/template/debian/common_translation.wml:16
msgid "debian, GNU, linux, unix, open source, free, DFSG"
msgstr ""

#: ../../english/template/debian/common_translation.wml:19
msgid "Back to the <a href=\"m4_HOME/\">Debian Project homepage</a>."
msgstr "Kembali ke <a href=\"m4_HOME/\">Halaman Utama Proyek Debian</a>."

#: ../../english/template/debian/common_translation.wml:22
#: ../../english/template/debian/links.tags.wml:149
msgid "Home"
msgstr "Halaman Utama"

#: ../../english/template/debian/common_translation.wml:25
msgid "Skip Quicknav"
msgstr ""

#: ../../english/template/debian/common_translation.wml:28
msgid "About"
msgstr "Tentang"

#: ../../english/template/debian/common_translation.wml:31
msgid "About Debian"
msgstr "Tentang Debian"

#: ../../english/template/debian/common_translation.wml:34
msgid "Contact Us"
msgstr "Kontak kami"

#: ../../english/template/debian/common_translation.wml:37
#, fuzzy
msgid "Legal Info"
msgstr "Informasi Tentang Debian Rilis"

#: ../../english/template/debian/common_translation.wml:40
msgid "Data Privacy"
msgstr ""

#: ../../english/template/debian/common_translation.wml:43
msgid "Donations"
msgstr "Donasi"

#: ../../english/template/debian/common_translation.wml:46
msgid "Events"
msgstr "Acara"

#: ../../english/template/debian/common_translation.wml:49
msgid "News"
msgstr "Berita"

#: ../../english/template/debian/common_translation.wml:52
msgid "Distribution"
msgstr "Distribusi"

#: ../../english/template/debian/common_translation.wml:55
msgid "Support"
msgstr ""

#: ../../english/template/debian/common_translation.wml:58
msgid "Pure Blends"
msgstr ""

#: ../../english/template/debian/common_translation.wml:61
#: ../../english/template/debian/links.tags.wml:46
msgid "Developers' Corner"
msgstr "Pojok Developer"

#: ../../english/template/debian/common_translation.wml:64
msgid "Documentation"
msgstr "Dokumentasi"

#: ../../english/template/debian/common_translation.wml:67
msgid "Security Information"
msgstr "Keamanan"

#: ../../english/template/debian/common_translation.wml:70
msgid "Search"
msgstr "Cari"

#: ../../english/template/debian/common_translation.wml:73
msgid "none"
msgstr "tidak&nbsp;ada"

#: ../../english/template/debian/common_translation.wml:76
msgid "Go"
msgstr "Cari"

#: ../../english/template/debian/common_translation.wml:79
msgid "worldwide"
msgstr "global"

#: ../../english/template/debian/common_translation.wml:82
msgid "Site map"
msgstr "Peta situs"

#: ../../english/template/debian/common_translation.wml:85
msgid "Miscellaneous"
msgstr "Lain-lain"

#: ../../english/template/debian/common_translation.wml:88
#: ../../english/template/debian/links.tags.wml:104
msgid "Getting Debian"
msgstr "Memperoleh Debian"

#: ../../english/template/debian/common_translation.wml:91
#, fuzzy
msgid "The Debian Blog"
msgstr "Buku-buku Tentang Debian"

#: ../../english/template/debian/common_translation.wml:94
#, fuzzy
msgid "Debian Micronews"
msgstr "Proyek Debian"

#: ../../english/template/debian/common_translation.wml:97
#, fuzzy
#| msgid "Debian Project"
msgid "Debian Planet"
msgstr "Proyek Debian"

#: ../../english/template/debian/common_translation.wml:100
#, fuzzy
msgid "Last Updated"
msgstr "Modifikasi terakhir"

#: ../../english/template/debian/ddp.wml:6
msgid ""
"Please send all comments, criticisms and suggestions about these web pages "
"to our <a href=\"mailto:debian-doc@lists.debian.org\">mailing list</a>."
msgstr ""
"Harap kirim semua komentar, kritik dan saran anda tentang situs web ini ke "
"<a href=\"mailto:debian-doc@lists.debian.org\">milis</a> kami."

#: ../../english/template/debian/fixes_link.wml:11
msgid "not needed"
msgstr "tidak diperlukan"

#: ../../english/template/debian/fixes_link.wml:14
msgid "not available"
msgstr "tidak tersedia"

#: ../../english/template/debian/fixes_link.wml:17
msgid "N/A"
msgstr "tidak tersedia"

#: ../../english/template/debian/fixes_link.wml:20
#, fuzzy
msgid "in release 1.1"
msgstr "di Debian 1.3"

#: ../../english/template/debian/fixes_link.wml:23
msgid "in release 1.3"
msgstr "di Debian 1.3"

#: ../../english/template/debian/fixes_link.wml:26
msgid "in release 2.0"
msgstr "di Debian 2.0"

#: ../../english/template/debian/fixes_link.wml:29
msgid "in release 2.1"
msgstr "di Debian 2.1"

#: ../../english/template/debian/fixes_link.wml:32
msgid "in release 2.2"
msgstr "di Debian 2.2"

#. TRANSLATORS: Please make clear in the translation of the following
#. item that mail sent to the debian-www list *must* be in English. Also,
#. you can add some information of your own translation mailing list
#. (i.e. debian-l10n-XXXXXX@lists.debian.org) for reporting things in
#. your language.
#: ../../english/template/debian/footer.wml:89
msgid ""
"To report a problem with the web site, please e-mail our publicly archived "
"mailing list <a href=\"mailto:debian-www@lists.debian.org\">debian-www@lists."
"debian.org</a> in English.  For other contact information, see the Debian <a "
"href=\"m4_HOME/contact\">contact page</a>. Web site source code is <a href="
"\"https://salsa.debian.org/webmaster-team/webwml\">available</a>."
msgstr ""

#: ../../english/template/debian/footer.wml:92
msgid "Last Modified"
msgstr "Modifikasi terakhir"

#: ../../english/template/debian/footer.wml:95
msgid "Last Built"
msgstr ""

#: ../../english/template/debian/footer.wml:98
msgid "Copyright"
msgstr "Hak Cipta"

#: ../../english/template/debian/footer.wml:101
msgid "<a href=\"https://www.spi-inc.org/\">SPI</a> and others;"
msgstr ""

#: ../../english/template/debian/footer.wml:104
msgid "See <a href=\"m4_HOME/license\" rel=\"copyright\">license terms</a>"
msgstr "Lihat <a href=\"m4_HOME/license\" rel=\"copyright\">Lisensi</a>"

#: ../../english/template/debian/footer.wml:107
msgid ""
"Debian is a registered <a href=\"m4_HOME/trademark\">trademark</a> of "
"Software in the Public Interest, Inc."
msgstr ""

#: ../../english/template/debian/languages.wml:196
#: ../../english/template/debian/languages.wml:232
#, fuzzy
msgid "This page is also available in the following languages:"
msgstr "Halaman ini juga tersedia dalam bahasa"

#: ../../english/template/debian/languages.wml:265
msgid "How to set <a href=m4_HOME/intro/cn>the default document language</a>"
msgstr ""
"Cara menentukan <a href=m4_HOME/intro/cn>bahasa utama untuk digunakan "
"browser anda</a>"

#: ../../english/template/debian/languages.wml:323
msgid "Browser default"
msgstr ""

#: ../../english/template/debian/languages.wml:323
msgid "Unset the language override cookie"
msgstr ""

#: ../../english/template/debian/links.tags.wml:4
msgid "Debian International"
msgstr "Debian Internasional"

#: ../../english/template/debian/links.tags.wml:7
msgid "Partners"
msgstr "Rekanan"

#: ../../english/template/debian/links.tags.wml:10
msgid "Debian Weekly News"
msgstr ""

#: ../../english/template/debian/links.tags.wml:13
msgid "Weekly News"
msgstr "Berita Mingguan"

#: ../../english/template/debian/links.tags.wml:16
#, fuzzy
msgid "Debian Project News"
msgstr "Proyek Debian"

#: ../../english/template/debian/links.tags.wml:19
#, fuzzy
msgid "Project News"
msgstr "Berita Mingguan"

#: ../../english/template/debian/links.tags.wml:22
msgid "Release Info"
msgstr "Informasi Tentang Debian Rilis"

#: ../../english/template/debian/links.tags.wml:25
msgid "Debian Packages"
msgstr "Paket-paket Debian"

#: ../../english/template/debian/links.tags.wml:28
msgid "Download"
msgstr ""

#: ../../english/template/debian/links.tags.wml:31
msgid "Debian&nbsp;on&nbsp;CD"
msgstr "CD&nbsp;Debian"

#: ../../english/template/debian/links.tags.wml:34
msgid "Debian Books"
msgstr "Buku-buku Tentang Debian"

#: ../../english/template/debian/links.tags.wml:37
#, fuzzy
msgid "Debian Wiki"
msgstr "Sokong Debian"

#: ../../english/template/debian/links.tags.wml:40
msgid "Mailing List Archives"
msgstr "Arsip Milis"

#: ../../english/template/debian/links.tags.wml:43
msgid "Mailing Lists"
msgstr "Daftar Milis"

#: ../../english/template/debian/links.tags.wml:49
msgid "Social Contract"
msgstr "Kontrak Sosial"

#: ../../english/template/debian/links.tags.wml:52
msgid "Code of Conduct"
msgstr ""

#: ../../english/template/debian/links.tags.wml:55
msgid "Debian 5.0 - The universal operating system"
msgstr ""

#: ../../english/template/debian/links.tags.wml:58
msgid "Site map for Debian web pages"
msgstr "Peta situs web Debian"

#: ../../english/template/debian/links.tags.wml:61
msgid "Developer Database"
msgstr "Basis Data Developer"

#: ../../english/template/debian/links.tags.wml:64
#, fuzzy
msgid "Debian FAQ"
msgstr "Buku-buku Tentang Debian"

#: ../../english/template/debian/links.tags.wml:67
msgid "Debian Policy Manual"
msgstr ""

#: ../../english/template/debian/links.tags.wml:70
msgid "Developers' Reference"
msgstr "Referensi Untuk Developer"

#: ../../english/template/debian/links.tags.wml:73
msgid "New Maintainers' Guide"
msgstr "Panduan Bagi Pengelola Baru"

#: ../../english/template/debian/links.tags.wml:76
msgid "Release Critical Bugs"
msgstr "Bug Kritikal"

#: ../../english/template/debian/links.tags.wml:79
msgid "Lintian Reports"
msgstr "Laporan Lintian"

#: ../../english/template/debian/links.tags.wml:83
msgid "Archives for users' mailing lists"
msgstr "Arsip milis debian-user"

#: ../../english/template/debian/links.tags.wml:86
msgid "Archives for developers' mailing lists"
msgstr "Arsip milis debian-devel"

#: ../../english/template/debian/links.tags.wml:89
msgid "Archives for i18n/l10n mailing lists"
msgstr "Arsip milis debian-i18n"

#: ../../english/template/debian/links.tags.wml:92
msgid "Archives for ports' mailing lists"
msgstr "Arsip milis debian-ports"

#: ../../english/template/debian/links.tags.wml:95
#, fuzzy
msgid "Archives for mailing lists of the Bug tracking system"
msgstr "Arsip milis debian-ports"

#: ../../english/template/debian/links.tags.wml:98
msgid "Archives for miscellaneous mailing lists"
msgstr "Arsip milis debian-misc"

#: ../../english/template/debian/links.tags.wml:101
msgid "Free Software"
msgstr "Perangkat Lunak Free"

#: ../../english/template/debian/links.tags.wml:107
msgid "Development"
msgstr "Pengembangan"

#: ../../english/template/debian/links.tags.wml:110
msgid "Help Debian"
msgstr "Sokong Debian"

#: ../../english/template/debian/links.tags.wml:113
msgid "Bug reports"
msgstr ""

#: ../../english/template/debian/links.tags.wml:116
msgid "Ports/Architectures"
msgstr ""

#: ../../english/template/debian/links.tags.wml:119
msgid "Installation manual"
msgstr "Petunjuk Instalasi"

#: ../../english/template/debian/links.tags.wml:122
msgid "CD vendors"
msgstr ""

#: ../../english/template/debian/links.tags.wml:125
msgid "CD/USB ISO images"
msgstr ""

#: ../../english/template/debian/links.tags.wml:128
msgid "Network install"
msgstr ""

#: ../../english/template/debian/links.tags.wml:131
msgid "Pre-installed"
msgstr ""

#: ../../english/template/debian/links.tags.wml:134
#, fuzzy
msgid "Debian-Edu project"
msgstr "Proyek Debian"

#: ../../english/template/debian/links.tags.wml:137
msgid "Alioth &ndash; Debian GForge"
msgstr ""

#: ../../english/template/debian/links.tags.wml:140
msgid "Quality Assurance"
msgstr ""

#: ../../english/template/debian/links.tags.wml:143
msgid "Package Tracking System"
msgstr ""

#: ../../english/template/debian/links.tags.wml:146
msgid "Debian Developer's Packages Overview"
msgstr ""

#: ../../english/template/debian/navbar.wml:10
#, fuzzy
msgid "Debian Home"
msgstr "Proyek Debian"

#: ../../english/template/debian/recent_list.wml:7
msgid "No items for this year."
msgstr "Untuk tahun ini tidak ada"

#: ../../english/template/debian/recent_list.wml:11
msgid "proposed"
msgstr "telah diajukan"

#: ../../english/template/debian/recent_list.wml:15
#, fuzzy
msgid "in discussion"
msgstr "Sedang&nbsp;Didiskusikan"

#: ../../english/template/debian/recent_list.wml:19
#, fuzzy
msgid "voting open"
msgstr "dipilih pada"

#: ../../english/template/debian/recent_list.wml:23
msgid "finished"
msgstr "selesai"

#: ../../english/template/debian/recent_list.wml:26
msgid "withdrawn"
msgstr ""

#: ../../english/template/debian/recent_list.wml:30
msgid "Future events"
msgstr "Acara mendatang"

#: ../../english/template/debian/recent_list.wml:33
msgid "Past events"
msgstr "Acara yang lalu"

#: ../../english/template/debian/recent_list.wml:37
msgid "(new revision)"
msgstr ""

#: ../../english/template/debian/recent_list.wml:329
#, fuzzy
msgid "Report"
msgstr "Dilaporkan Pada"

#: ../../english/template/debian/redirect.wml:6
msgid "Page redirected to <newpage/>"
msgstr ""

#: ../../english/template/debian/redirect.wml:12
msgid ""
"This page has been renamed to <url <newpage/>>, please update your links."
msgstr ""

#. given a manual name and an architecture, join them
#. if you need to reorder the two, use "%2$s ... %1$s", cf. printf(3)
#: ../../english/template/debian/release.wml:7
msgid "<void id=\"doc_for_arch\" />%s for %s"
msgstr "<void id=\"doc_for_arch\" />%s untuk %s"

#: ../../english/template/debian/translation-check.wml:37
msgid ""
"<em>Note:</em> The <a href=\"$link\">original document</a> is newer than "
"this translation."
msgstr ""
"<em>Catatan:</em> Dokumen ini sudah kadaluwarsa, yang <a href=\"$link"
"\">asli</a> telah dimodifikasi."

#: ../../english/template/debian/translation-check.wml:43
msgid ""
"Warning! This translation is too out of date, please see the <a href=\"$link"
"\">original</a>."
msgstr ""
"Peringatan! Translasi untuk dokumen ini sudah terlalu kadaluwarsa, harap "
"lihat dokumen <a href=\"$link\">asli</a>."

#: ../../english/template/debian/translation-check.wml:49
msgid ""
"<em>Note:</em> The original document of this translation no longer exists."
msgstr ""
"<em>Catatan:</em> Dokumen asli untuk terjemahan ini sudah tidak ada lagi."

#: ../../english/template/debian/translation-check.wml:56
msgid "Wrong translation version!"
msgstr ""

#: ../../english/template/debian/url.wml:4
msgid "URL"
msgstr ""

#: ../../english/template/debian/users.wml:12
#, fuzzy
msgid "Back to the <a href=\"../\">Who's using Debian? page</a>."
msgstr "Kembali ke <a href=\"./\">halaman Penasehat</a>."

#~ msgid "Select a server near you"
#~ msgstr "Pilih server terdekat"

#~ msgid "List of Consultants"
#~ msgstr "Daftar Penasehat"

#~ msgid "Back to the <a href=\"./\">Debian consultants page</a>."
#~ msgstr "Kembali ke <a href=\"./\">halaman Penasehat</a>."

#, fuzzy
#~ msgid "Nominations"
#~ msgstr "Donasi"

#, fuzzy
#~ msgid "Proposer"
#~ msgstr "telah diajukan"

#, fuzzy
#~ msgid "Minimum Discussion"
#~ msgstr "Sedang&nbsp;Didiskusikan"

#~ msgid "Waiting&nbsp;for&nbsp;Sponsors"
#~ msgstr "Menunggu&nbsp;Sponsor"

#~ msgid "In&nbsp;Discussion"
#~ msgstr "Sedang&nbsp;Didiskusikan"

#~ msgid "Voting&nbsp;Open"
#~ msgstr "Pemungutan&nbsp;Suara&nbsp;Berjalan"

#~ msgid "Decided"
#~ msgstr "Telah&nbsp;Diputuskan"

#~ msgid "Other"
#~ msgstr "Lain-lain"

#~ msgid "Home&nbsp;Vote&nbsp;Page"
#~ msgstr "Halaman&nbsp;Utama&nbsp;Pemungutan&nbsp;Suara"

#~ msgid "How&nbsp;To"
#~ msgstr "Bagaimana"

#~ msgid "Submit&nbsp;a&nbsp;Proposal"
#~ msgstr "Mengajukan&nbsp;Proposal"

#~ msgid "Amend&nbsp;a&nbsp;Proposal"
#~ msgstr "Mengubah&nbsp;Proposal"

#~ msgid "Follow&nbsp;a&nbsp;Proposal"
#~ msgstr "Menindaklanjuti&nbsp;Proposal"

#~ msgid "Read&nbsp;a&nbsp;Result"
#~ msgstr "Lihat&nbsp;Hasil"

#~ msgid "Vote"
#~ msgstr "Pilih"

#~ msgid "%0 (dead link)"
#~ msgstr "%0 (link buntu)"

#~ msgid ""
#~ "See the Debian <a href=\"m4_HOME/contact\">contact page</a> for "
#~ "information on contacting us."
#~ msgstr ""
#~ "Untuk menghubungi kami, silahkan lihat <a href=\"m4_HOME/contact"
#~ "\">Halaman kontak</a> Debian."

#~ msgid "discussed"
#~ msgstr "telah didiskusikan"

#~ msgid "Vulnerable"
#~ msgstr "Rawan"

#~ msgid "Fixed in"
#~ msgstr "Telah diperbaiki di"

#~ msgid "Source:"
#~ msgstr "Sumber"

#~ msgid "Architecture-independent component:"
#~ msgstr "Komponen yang Arsitektur-independen:"

#, fuzzy
#~ msgid ""
#~ "MD5 checksums of the listed files are available in the <a href=\"<get-var "
#~ "url />\">original advisory</a>."
#~ msgstr ""
#~ "Checksum MD5 untuk file-file terdaftar tersedia di <a href=\"%attributes"
#~ "\">original advisory</a>."

#~ msgid ""
#~ "Debian Weekly News is edited by <a href=\"mailto:dwn@debian.org\">Joe "
#~ "'Zonker' Brockmeier, Jean-Christophe Helary and Tollef Fog Heen</a>."
#~ msgstr ""
#~ "Debian Weekly News diedit oleh <a href=\"mailto:dwn@debian.org\">Joe "
#~ "'Zonker' Brockmeier, Jean-Christophe Helary and Tollef Fog Heen</a>."

#~ msgid ""
#~ "Debian Weekly News is edited by <a href=\"mailto:dwn@debian.org\">Joe "
#~ "'Zonker' Brockmeier and Martin 'Joey' Schulze</a>."
#~ msgstr ""
#~ "Debian Weekly News diedit oleh <a href=\"mailto:dwn@debian.org\">Joe "
#~ "'Zonker' Brockmeier and Martin 'Joey' Schulze</a>."

#, fuzzy
#~ msgid ""
#~ "Debian Weekly News is edited by <a href=\"mailto:dwn@debian.org"
#~ "\">Yooseong Yang and Martin 'Joey' Schulze</a>."
#~ msgstr ""
#~ "Debian Weekly News diedit oleh <a href=\"mailto:dwn@debian.org\">Joe "
#~ "'Zonker' Brockmeier and Martin 'Joey' Schulze</a>."

#, fuzzy
#~ msgid ""
#~ "Debian Weekly News is edited by <a href=\"mailto:dwn@debian.org\">Martin "
#~ "'Joey' Schulze</a>."
#~ msgstr ""
#~ "Debian Weekly News diedit oleh <a href=\"mailto:dwn@debian.org\">Joey "
#~ "Hess</a>."

#, fuzzy
#~ msgid "non-free"
#~ msgstr "tidak&nbsp;ada"

#, fuzzy
#~ msgid "Discussion"
#~ msgstr "Sedang&nbsp;Didiskusikan"

#, fuzzy
#~ msgid "License Information"
#~ msgstr "Informasi lebih lanjut"

#, fuzzy
#~ msgid "No Requested packages"
#~ msgstr "Paket-paket Terkena Efek"

#, fuzzy
#~ msgid "No requests for adoption"
#~ msgstr "Informasi lebih lanjut"

#, fuzzy
#~ msgid ""
#~ "<void id=\"plural\" />This issue of Debian Weekly News was edited by <a "
#~ "href=\"mailto:dwn@debian.org\">%s</a>."
#~ msgstr ""
#~ "Debian Weekly News diedit oleh <a href=\"mailto:dwn@debian.org\">Joey "
#~ "Hess</a>."

#, fuzzy
#~ msgid ""
#~ "<void id=\"singular\" />This issue of Debian Weekly News was edited by <a "
#~ "href=\"mailto:dwn@debian.org\">%s</a>."
#~ msgstr ""
#~ "Debian Weekly News diedit oleh <a href=\"mailto:dwn@debian.org\">Joey "
#~ "Hess</a>."

#, fuzzy
#~ msgid ""
#~ "<void id=\"plural\" />Debian Weekly News is edited by <a href=\"mailto:"
#~ "dwn@debian.org\">%s</a>."
#~ msgstr ""
#~ "Debian Weekly News diedit oleh <a href=\"mailto:dwn@debian.org\">Joey "
#~ "Hess</a>."

#, fuzzy
#~ msgid ""
#~ "<void id=\"singular\" />Debian Weekly News is edited by <a href=\"mailto:"
#~ "dwn@debian.org\">%s</a>."
#~ msgstr ""
#~ "Debian Weekly News diedit oleh <a href=\"mailto:dwn@debian.org\">Joey "
#~ "Hess</a>."

#, fuzzy
#~ msgid ""
#~ "To receive this newsletter weekly in your mailbox, <a href=\"http://lists."
#~ "debian.org/debian-news/\">subscribe to the debian-news mailing list</a>."
#~ msgstr ""
#~ "Untuk menerima newsletter mingguan ini di email anda, silahkan <a href="
#~ "\"m4_HOME/MailingLists/subscribe#debian-news\">subscribe</a> ke milis "
#~ "debian-news."

#, fuzzy
#~ msgid "Back to the <a href=\"./\">Debian speakers page</a>."
#~ msgstr "Kembali ke <a href=\"./\">halaman Penasehat</a>."

#, fuzzy
#~ msgid ""
#~ "<void id=\"plural\" />This issue of Debian Project News was edited by <a "
#~ "href=\"mailto:debian-publicity@lists.debian.org\">%s</a>."
#~ msgstr ""
#~ "Debian Weekly News diedit oleh <a href=\"mailto:dwn@debian.org\">Joey "
#~ "Hess</a>."

#, fuzzy
#~ msgid ""
#~ "<void id=\"singular\" />This issue of Debian Project News was edited by "
#~ "<a href=\"mailto:debian-publicity@lists.debian.org\">%s</a>."
#~ msgstr ""
#~ "Debian Weekly News diedit oleh <a href=\"mailto:dwn@debian.org\">Joey "
#~ "Hess</a>."

#, fuzzy
#~ msgid ""
#~ "<void id=\"plural\" />Debian Project News is edited by <a href=\"mailto:"
#~ "debian-publicity@lists.debian.org\">%s</a>."
#~ msgstr ""
#~ "Debian Weekly News diedit oleh <a href=\"mailto:dwn@debian.org\">Joey "
#~ "Hess</a>."

#, fuzzy
#~ msgid ""
#~ "<void id=\"singular\" />Debian Project News is edited by <a href=\"mailto:"
#~ "debian-publicity@lists.debian.org\">%s</a>."
#~ msgstr ""
#~ "Debian Weekly News diedit oleh <a href=\"mailto:dwn@debian.org\">Joey "
#~ "Hess</a>."

#~ msgid "<a href=\"../../\">Back issues</a> of this newsletter are available."
#~ msgstr "Edisi lama newsletter ini tersedia di <a href=\"../../\">sini</a>."

#, fuzzy
#~ msgid ""
#~ "To receive this newsletter bi-weekly in your mailbox, <a href=\"http://"
#~ "lists.debian.org/debian-news/\">subscribe to the debian-news mailing "
#~ "list</a>."
#~ msgstr ""
#~ "Untuk menerima newsletter mingguan ini di email anda, silahkan <a href="
#~ "\"m4_HOME/MailingLists/subscribe#debian-news\">subscribe</a> ke milis "
#~ "debian-news."

#, fuzzy
#~ msgid ""
#~ "Back to: other <a href=\"./\">Debian news</a> || <a href=\"m4_HOME/"
#~ "\">Debian Project homepage</a>."
#~ msgstr "Kembali ke <a href=\"m4_HOME/\">Halaman Utama Proyek Debian</a>."

#~ msgid "Latest News"
#~ msgstr "Berita Terbaru"

#~ msgid "Related Links"
#~ msgstr "Info Lainnya"

#~ msgid "<th>Project</th><th>Coordinator</th>"
#~ msgstr "<th>Koordinator</th><th>Proyek</th>"

#~ msgid "Main Coordinator"
#~ msgstr "Koordinator Utama"

#~ msgid "Debian Involvement"
#~ msgstr "Partisipasi Debian"

#~ msgid "More Info"
#~ msgstr "Informasi lebih lanjut"

#~ msgid "Where"
#~ msgstr "Tempat"

#~ msgid "When"
#~ msgstr "Waktu"

#~ msgid "link may no longer be valid"
#~ msgstr "link ini mungkin sudah kadaluwarsa"

#~ msgid "Upcoming Attractions"
#~ msgstr "Acara mendatang"

#~ msgid "More information"
#~ msgstr "Informasi lebih lanjut"

#~ msgid "More information:"
#~ msgstr "Informasi lainnya:"

#~ msgid "Visit the site sponsor"
#~ msgstr "Sponsor situs web"
