#use wml::debian::template title="데비안 &ldquo;bullseye&rdquo; 릴리스 정보"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/bullseye/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="c8265f664931f5ce551446ba751d63d63b70ce86" maintainer="Seunghun Han (kkamagui)"

<if-stable-release release="bullseye">

<p>데비안 <current_release_bullseye>가
<a href="$(HOME)/News/<current_release_newsurl_bullseye/>">\
<current_release_date_bullseye></a>에 출시되었습니다.
<ifneq "11.0" "<current_release>"
  "Debian 11.0 was initially released on <:=spokendate('XXXXXXXX'):>."
/>
릴리스에는 주요 수정사항이 많이 들어 있으며,
우리 <a href="$(HOME)/News/XXXX/XXXXXXXX">보도 자료</a>와
<a href="releasenotes">릴리스 노트</a>에 설명되어 있습니다.</p>

#<p><strong>Debian 11 has been superseded by
#<a href="../bookworm/">Debian 12 (<q>bookworm</q>)</a>.
#Security updates have been discontinued as of <:=spokendate('xxxx-xx-xx'):>.
#</strong></p>

### This paragraph is orientative, please review before publishing!
#<p><strong>However, bullseye benefits from Long Term Support (LTS) until
#the end of xxxxx 20xx. The LTS is limited to i386, amd64, armel, armhf and arm64.
#All other architectures are no longer supported in bullseye.
#For more information, please refer to the <a
#href="https://wiki.debian.org/LTS">LTS section of the Debian Wiki</a>.
#</strong></p>

<p>데비안을 구해 설치하려면, <a href="debian-installer/">설치 정보</a>
페이지와 <a href="installmanual">설치 가이드</a>를 보세요. 이전 데비안
릴리스에서 업그레이드를 하려면, <a href="releasenotes">릴리스 노트</a>에 있는
절차를 보세요.</p>

### Activate the following when LTS period starts.
#<p>Architectures supported during Long Term Support:</p>
#
#<ul>
#<:
#foreach $arch (@archeslts) {
#	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
#}
#:>
#</ul>

<p>Bullseye의 최초 릴리스에서 지원하는 컴퓨터 아키텍처:</p>

<ul>
<:
foreach $arch (@arches) {
	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>

<p>우리의 바람과 달리 릴리스에 몇몇 문제가 있을 수 있습니다.
<em>안정(stable)</em> 버전으로 선언되었음에도 말이죠. 그래서
<a href="errata">알려진 주요 문제 목록</a>을 만들었으며, 여러분은 언제나
<a href="reportingbugs">다른 이슈를 보고</a>할 수 있습니다.</p>

<p>끝으로 중요한 점은, 릴리스를 하는데 <a href="credits">공적을 세운 분들</a>의
리스트를 만들었다는 겁니다.</p>
</if-stable-release>

<if-stable-release release="buster">

<p><a href="../buster/">buster</a>의 차기 데비안 릴리스용 코드명이
<q>bullseye</q>입니다.</p>

<p>해당 릴리스는 buster의 사본에서 시작되었고, 현재
<q><a href="$(DOC)/manuals/debian-faq/ftparchives#testing">테스트(testing)</a></q>
상태입니다.
이는 불안정(unstable)이나 실험(experimental) 배포판만큼 나빠서 망가질 우려가
없다는 걸 뜻합니다. 이 배포판에 패키지가 포함되려면 일정 기간이 지나야 하고,
릴리스에 반하는 심각한 릴리스 버그를 포함하지 않을 때만 가능하기 때문이죠.</p>

<p><q>테스트(testing)</q> 배포판은 보안팀이 아직 보안 업데이트를 <strong>하지
않는다는 점</strong>을 명심하세요. 따라서, <q>테스트(testing)</q>는 적시에
보안 업데이트를 <strong>받을 수 없습니다</strong>.
# For more information please see the
# <a href="https://lists.debian.org/debian-testing-security-announce/2008/12/msg00019.html">announcement</a>
# of the Testing Security Team.
보안 지원이 필요하다면 여러분의 sources.list 항목을 테스트(testing)에서
buster로 바꿔야 합니다.
<q>테스트(testing)</q> 배포판의 <a href="$(HOME)/security/faq#testing">보안 팀
FAQ</a>항목도 함께 보세요.</p>

<p><a href="releasenotes">접근 가능한 릴리스 노트의 초안</a>이 있을 지도
모릅니다. <a href="https://bugs.debian.org/release-notes">릴리스 노트의 제안된
추가 항목들도 확인</a>하세요.</p>

<p><q>테스트(testing)</q> 배포판을 설치하는 방법과 관련된 설치 이미지와 문서는
<a href="$(HOME)/devel/debian-installer/">데비안 설치관리자 페이지</a>를
보세요.</p>

<p><q>테스트(testing)</q> 배포판이 동작하는 방식과 관련된 더 많은 정보를
찾는다면, <a href="$(HOME)/devel/testing">개발자 정보</a>를 확인하세요.</p>

<p>사람들은 종종 하나로 된 릴리스 <q>진행 측정기</q>가 있는지 묻습니다.
안타깝게도 하나로 된 것은 없지만, 릴리스가 될 때 같이 처리되어야할 것들 기록된
몇몇 위치를 알려드릴 순 있습니다.</p>

<ul>
  <li><a href="https://release.debian.org/">일반 릴리스 상태 페이지</a></li>
  <li><a href="https://bugs.debian.org/release-critical/">심각한 릴리스 버그</a></li>
  <li><a href="https://udd.debian.org/bugs.cgi?base=only&amp;rc=1">기반 시스템 버그</a></li>
  <li><a href="https://udd.debian.org/bugs.cgi?standard=only&amp;rc=1">표준(standard) 및 태스크(task) 패키지의 버그</a></li>
</ul>

<p>또한, 일반 상태 보고는 릴리스 매니저가
<a href="https://lists.debian.org/debian-devel-announce/">debian-devel-announce
메일링 리스트</a>에 게시합니다.</p>

</if-stable-release>
