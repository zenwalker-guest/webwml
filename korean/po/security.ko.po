# Security
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# Song Woo-il, 2006
# Sangdo Jun <sebuls@gmail.com>, 2020
msgid ""
msgstr ""
"Project-Id-Version: Debian Security\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2020-06-12 22:34+0900\n"
"Last-Translator: Sangdo Jun <sebuls@gmail.com>\n"
"Language-Team: debian-l10n-korean <debian-l10n-korean@lists.debian.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.2.1\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: ../../english/security/dsa.rdf.in:16
msgid "Debian Security"
msgstr "데비안 보안"

#: ../../english/security/dsa.rdf.in:19
msgid "Debian Security Advisories"
msgstr "데비안 보안 권고"

#: ../../english/security/faq.inc:6
msgid "Q"
msgstr "질문"

#: ../../english/security/index.include:17
msgid ""
"<a href=\\\"undated/\\\">undated</a> security advisories, included for "
"posterity"
msgstr "<a href=\\\"undated/\\\">날짜없는</a> 보안 권고, 후세를 위해 포함"

#: ../../english/security/make-ref-table.pl:81
msgid "Mitre CVE dictionary"
msgstr "Mitre CVE 사전"

#: ../../english/security/make-ref-table.pl:84
msgid "Securityfocus Bugtraq database"
msgstr "Securityfocus Bugtraq 데이터베이스"

#: ../../english/security/make-ref-table.pl:88
msgid "CERT Advisories"
msgstr "CERT 권고"

#: ../../english/security/make-ref-table.pl:92
msgid "US-CERT vulnerabilities Notes"
msgstr "US-CERT 취약성 노트"

#: ../../english/template/debian/security.wml:11
msgid "Source:"
msgstr "소스:"

#: ../../english/template/debian/security.wml:15
msgid "Architecture-independent component:"
msgstr "아키텍처에 의존하는 구성요소:"

#. don't translate `<get-var url />'
#: ../../english/template/debian/security.wml:22
msgid ""
"MD5 checksums of the listed files are available in the <a href=\"<get-var "
"url />\">original advisory</a>."
msgstr ""
"열거된 파일의 MD5 체크섬은 <a href=\"<get-var url />\">원본 권고</a>에서 구"
"할 수 있습니다."

#. don't translate `<get-var url />'
#: ../../english/template/debian/security.wml:30
msgid ""
"MD5 checksums of the listed files are available in the <a href=\"<get-var "
"url />\">revised advisory</a>."
msgstr ""
"열거된 파일의 MD5 체크섬은 <a href=\"<get-var url />\">개정된 권고</a>에서 구"
"할 수 있습니다."

#: ../../english/template/debian/security.wml:44
msgid "Debian Security Advisory"
msgstr "데비안 보안 권고"

#: ../../english/template/debian/security.wml:49
msgid "Date Reported"
msgstr "보고일"

#: ../../english/template/debian/security.wml:52
msgid "Affected Packages"
msgstr "영향 받는 패키지"

#: ../../english/template/debian/security.wml:74
msgid "Vulnerable"
msgstr "위험성"

#: ../../english/template/debian/security.wml:77
msgid "Security database references"
msgstr "보안 데이터베이스 참조"

#: ../../english/template/debian/security.wml:80
msgid "More information"
msgstr "추가 정보"

#: ../../english/template/debian/security.wml:86
msgid "Fixed in"
msgstr "수정"

#: ../../english/template/debian/securityreferences.wml:16
msgid "BugTraq ID"
msgstr "BugTraq ID"

#: ../../english/template/debian/securityreferences.wml:60
msgid "Bug"
msgstr "버그"

#: ../../english/template/debian/securityreferences.wml:76
msgid "In the Debian bugtracking system:"
msgstr "데비안 버그 추적 시스템:"

#: ../../english/template/debian/securityreferences.wml:79
msgid "In the Bugtraq database (at SecurityFocus):"
msgstr "SecurityFocus의 Bugtraq 데이터베이스:"

#: ../../english/template/debian/securityreferences.wml:82
msgid "In Mitre's CVE dictionary:"
msgstr "Mitre의 CVE 사전:"

#: ../../english/template/debian/securityreferences.wml:85
msgid "CERT's vulnerabilities, advisories and incident notes:"
msgstr "CERT의 취약성, 자문 및 문제 참고 사항:"

#: ../../english/template/debian/securityreferences.wml:88
msgid "No other external database security references currently available."
msgstr "현재 사용 가능한 다른 외부 데이터베이스 보안 참조가 없습니다."
