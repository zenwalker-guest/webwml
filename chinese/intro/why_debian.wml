#use wml::debian::template title="选择 Debian 的理由" MAINPAGE="true"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="7141cf5f25b03b1aa3c02c1afe2a5ca8cab2f672" maintainer="Vifly"

<p>用户选择 Debian 作为他们的操作系统的原因有很多。</p>

<h1>主要的原因</h1>

# try to list reasons for end users first

<dl>
  <dt><strong>Debian 是自由软件。</strong></dt>
  <dd>
    Debian 是由自由和开放源代码的软件组成的，并将始终保持100%<a href="free">自由</a>。\
    每个人都能自由使用、修改，以及分发。这是我们对<a href="../users">我们的用户</a>的主要\
    承诺。它也是免费的。
  </dd>
</dl>

<dl>
  <dt><strong>Debian 是一个稳定且安全的基于 Linux 的操作系统。</strong></dt>
  <dd>
    Debian 是一个广泛用于各种设备的操作系统，其使用范围包括笔记本电脑，台式机和服务器。\
    自1993年以来，它的稳定性和可靠性就深受用户的喜爱。我们为每个软件包提供合理的默认配置。\
    Debian 开发人员会尽可能在其生命周期内为所有软件包提供安全更新。
  </dd>
</dl>

<dl>
  <dt><strong>Debian 具有广泛的硬件支持。</strong></dt>
  <dd>
    大多数硬件已获得 Linux 内核的支持。当自由软件无法提供足够的支持时，也可使用专用的硬件\
    驱动程序。
  </dd>
</dl>

<dl>
  <dt><strong>Debian 提供平滑的更新。</strong></dt>
  <dd>
    Debian 以在其发行周期内轻松流畅地进行更新而闻名，不仅如此，还包括轻松升级到下一个大版本。
  </dd>
</dl>

<dl>
  <dt><strong>Debian 是许多其他发行版的种子和基础。</strong></dt>
  <dd>
    许多非常受欢迎的 Linux 发行版，例如 Ubuntu、Knoppix、PureOS、SteamOS 以及 Tails，都\
    选择了 Debian 作为它们的软件基础。Debian 提供了所有工具，因此每个人都可以用能满足自己需求\
    的软件包来扩展 Debian 档案库中的软件包。
  </dd>
</dl>

<dl>
  <dt><strong>Debian 项目是一个社区。</strong></dt>
  <dd>
    Debian 不只是一个 Linux 操作系统。该软件由来自世界各地的数百名志愿者共同制作。即使您\
    不是一个程序员或系统管理员，也可以成为 Debian 社区的一员。Debian 是由社区和共识驱动的，\
    具有一个<a href="../devel/constitution">民主的治理架构</a>。由于所有 Debian 开发\
    人员都享有平等的权利，所以它不能被单个公司所控制。我们的开发人员遍布在60多个国家/地区，\
    并且 Debian Installer 提供了超过80种语言的翻译支持。
  </dd>
</dl>

<dl>
  <dt><strong>Debian 有多个安装程序选项。</strong></dt>
  <dd>
    终端用户会使用我们的\
    <a href="https://cdimage.debian.org/images/release/current-live/amd64/iso-hybrid/"> Live CD</a>，\
    其包括易于使用的 Calamares 安装程序，只需很少的输入或前置知识。经验丰富的用户可以使用我们\
    独特的功能齐全的安装程序，而专家可以对安装进行微调，甚至可以使用自动网络安装工具。
  </dd>
</dl>

<br>

<h1>企业环境</h1>

<p>
  如果您在专业环境中需要使用 Debian，则可以享受以下额外好处：
</p>

<dl>
  <dt><strong>Debian 是可靠的。</strong></dt>
  <dd>
    Debian 在从单个用户的笔记本到超级对撞机、证劵交易所和汽车行业的数以千计的现实日常场景\
    中证明其可靠性。它在学术界、科研机构和公共部门中也很流行。
  </dd>
</dl>

<dl>
  <dt><strong>Debian 有很多专家。</strong></dt>
  <dd>
    我们的软件包维护者不仅会为 Debian 软件包修正问题，还会将其补丁合并到新的上游版本。他们\
    通常是上游软件的专家，直接为上游开发做出贡献。有时他们也是上游的一部分开发成员。
  </dd>
</dl>

<dl>
  <dt><strong>Debian 是安全的。</strong></dt>
  <dd>
    Debian 对其稳定版本提供安全支持。许多其它发行版的开发人员和安全研究人员都依赖 Debian \
    的安全跟踪器。
  </dd>
</dl>

<dl>
  <dt><strong>长期支持。</strong></dt>
  <dd>
    Debian 提供了免费的<a href="https://wiki.debian.org/LTS">长期支持</a> (LTS)。\
    这为您提供了对稳定版本至少5年的额外支持。除此以外，还有<a 
    href="https://wiki.debian.org/LTS/Extended">扩展的 LTS </a>计划，该计划将\
    对有限的软件包的支持延长到了5年以上。
  </dd>
</dl>

<dl>
  <dt><strong>云映像。</strong></dt>
  <dd>
    官方云映像可用于所有的主流云端平台。我们还提供了工具和配置，因此您可以构建自己的自定义\
    云映像。您还可以在桌面或容器里的虚拟机中使用 Debian。
  </dd>
</dl>

<br>

<h1>开发者</h1>
<p>Debian 被各种软件和硬件开发人员广泛使用。</p>

<dl>
  <dt><strong>公开的错误跟踪系统。</strong></dt>
  <dd>
    我们的 Debian <a href="../Bugs">错误跟踪系统</a> (BTS)向所有人公开，任何人都可\
    通过浏览器访问。我们不会隐藏我们的软件错误，您可以轻松提交新的错误报告。
  </dd>
</dl>

<dl>
  <dt><strong>物联网和嵌入式设备。</strong></dt>
  <dd>
    我们支持各种设备，例如 Raspberry Pi、QNAP的各个变种、移动设备、家庭路由器以及大量\
    单板计算机 (SBC)。
  </dd>
</dl>

<dl>
  <dt><strong>多种硬件架构支持。</strong></dt>
  <dd>
    Debian 支持<a href="../ports">一长串</a>的CPU架构，包括 AMD64、i386、ARM \
    和 MIPS 、POWER7、POWER8、IBM System z、RISC-V 的多个版本。Debian 还可以\
    用于较旧的特定架构。
  </dd>
</dl>

<dl>
  <dt><strong>为数众多的可用软件包。</strong></dt>
  <dd>
    Debian 拥有最多数量的已安装软件包（当前为 <packages_in_stable>）。我们的软件包\
    使用 deb 格式，该格式以其高质量着称。
  </dd>
</dl>

<dl>
  <dt><strong>提供不同版本的选择。</strong></dt>
  <dd>
    除了我们的稳定版本外，您还可以通过安装测试版或不稳定版本来获得最新版本的软件。
  </dd>
</dl>

<dl>
  <dt><strong>在开发人员工具和政策的帮助下保证高质量。</strong></dt>
  <dd>
    多个开发人员工具帮软件包维持高水平的质量，并且我们的<a 
    href="../doc/debian-policy/">政策</a>规定了每个被官方仓库接受的软件包所必须满足\
    的技术需求。我们的持续集成是运行在 autopkgtest 这个软件上的，piuparts 是我们的\
    安装、升级和删除测试工具，lintian 是一个用于 Debian 软件包的全面的软件包检查程序。
  </dd>
</dl>

<br>

<h1>我们的用户怎么说</h1>

<ul style="line-height: 3em">
  <li>
    <q><strong>
      对我而言，这是易用性和稳定性的完美水平。这些年来，我使用了各种不同的发行版，但是 \
      Debian 是唯一一个可以使用的发行版。
    </strong></q>
  </li>

  <li>
    <q><strong>
      坚如磐石的品质，提供了海量的软件包，还有优秀的社区。
    </strong></q>
  </li>

  <li>
    <q><strong>
      对我来说，Debian 是稳定和易于使用的代表。
    </strong></q>
  </li>
</ul>
