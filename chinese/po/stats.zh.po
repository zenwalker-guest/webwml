msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: \n"
"Last-Translator: Boyuan Yang <073plan@gmail.com>\n"
"Language-Team: \n"
"Language: zh\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.2.1\n"

#: ../../english/template/debian/stats_tags.wml:6
msgid "Debian web site translation statistics"
msgstr "Debian 網站翻譯統計"

#: ../../english/template/debian/stats_tags.wml:10
msgid "There are %d pages to translate."
msgstr "一共有 %d 個頁面需要翻譯。"

#: ../../english/template/debian/stats_tags.wml:14
msgid "There are %d bytes to translate."
msgstr "一共有 %d [CN:字節:][HKTW:位元组:]需要翻譯。"

#: ../../english/template/debian/stats_tags.wml:18
msgid "There are %d strings to translate."
msgstr "一共有 %d 個[CN:字符串:][HKTW:字串:]需要翻譯。"

#: ../../stattrans.pl:282 ../../stattrans.pl:498
msgid "Wrong translation version"
msgstr "譯文版本錯誤"

#: ../../stattrans.pl:284
msgid "This translation is too out of date"
msgstr "此譯文太過時了"

#: ../../stattrans.pl:286
msgid "The original is newer than this translation"
msgstr "原文比此譯文新"

#: ../../stattrans.pl:290 ../../stattrans.pl:498
msgid "The original no longer exists"
msgstr "原文不再存在"

#: ../../stattrans.pl:474
msgid "hit count N/A"
msgstr "命中计数 N/A"

#: ../../stattrans.pl:474
msgid "hits"
msgstr "命中"

#: ../../stattrans.pl:492 ../../stattrans.pl:493
msgid "Click to fetch diffstat data"
msgstr "点击以获取 diffstat 数据"

#: ../../stattrans.pl:603 ../../stattrans.pl:743
msgid "Created with <transstatslink>"
msgstr "使用 <transstatslink> 创建"

#: ../../stattrans.pl:608
msgid "Translation summary for"
msgstr "翻譯概要 for"

#: ../../stattrans.pl:611 ../../stattrans.pl:767 ../../stattrans.pl:813
#: ../../stattrans.pl:856
msgid "Not translated"
msgstr "未翻譯"

#: ../../stattrans.pl:611 ../../stattrans.pl:766 ../../stattrans.pl:812
msgid "Outdated"
msgstr "過時"

#: ../../stattrans.pl:611
msgid "Translated"
msgstr "已翻譯"

#: ../../stattrans.pl:611 ../../stattrans.pl:691 ../../stattrans.pl:765
#: ../../stattrans.pl:811 ../../stattrans.pl:854
msgid "Up to date"
msgstr "最新"

#: ../../stattrans.pl:612 ../../stattrans.pl:613 ../../stattrans.pl:614
#: ../../stattrans.pl:615
msgid "files"
msgstr "個[CN:文件:][HKTW:檔案:]"

#: ../../stattrans.pl:618 ../../stattrans.pl:619 ../../stattrans.pl:620
#: ../../stattrans.pl:621
msgid "bytes"
msgstr "[CN:字節:][HKTW:位元組:]"

#: ../../stattrans.pl:628
msgid ""
"Note: the lists of pages are sorted by popularity. Hover over the page name "
"to see the number of hits."
msgstr ""
"注意：页面列表使用其访问次数进行排序。在页面名称上悬停鼠标光标可以查看访问命"
"中次数。"

#: ../../stattrans.pl:634
msgid "Outdated translations"
msgstr "過時的翻譯"

#: ../../stattrans.pl:636 ../../stattrans.pl:690
msgid "File"
msgstr "[CN:文件:][HKTW:檔案:]"

#: ../../stattrans.pl:638
msgid "Diff"
msgstr "差异"

#: ../../stattrans.pl:640
msgid "Comment"
msgstr "備註"

#: ../../stattrans.pl:641
msgid "Diffstat"
msgstr "Diffstat"

#: ../../stattrans.pl:642
msgid "Git command line"
msgstr "Git 命令行"

#: ../../stattrans.pl:644
msgid "Log"
msgstr "日誌"

#: ../../stattrans.pl:645
msgid "Translation"
msgstr "譯文"

#: ../../stattrans.pl:646
msgid "Maintainer"
msgstr "維護者"

#: ../../stattrans.pl:648
msgid "Status"
msgstr "狀態"

#: ../../stattrans.pl:649
msgid "Translator"
msgstr "翻譯者"

#: ../../stattrans.pl:650
msgid "Date"
msgstr "日期"

#: ../../stattrans.pl:657
msgid "General pages not translated"
msgstr "未翻譯的一般頁面"

#: ../../stattrans.pl:658
msgid "Untranslated general pages"
msgstr "未翻譯的一般頁面"

#: ../../stattrans.pl:663
msgid "News items not translated"
msgstr "未翻譯的新聞"

#: ../../stattrans.pl:664
msgid "Untranslated news items"
msgstr "未翻譯的新聞"

#: ../../stattrans.pl:669
msgid "Consultant/user pages not translated"
msgstr "未翻譯的顧問/[CN:用戶:][HKTW:使用者:]頁面"

#: ../../stattrans.pl:670
msgid "Untranslated consultant/user pages"
msgstr "未翻譯的顧問/[CN:用戶:][HKTW:使用者:]頁面"

#: ../../stattrans.pl:675
msgid "International pages not translated"
msgstr "未翻譯的國際頁面"

#: ../../stattrans.pl:676
msgid "Untranslated international pages"
msgstr "未翻譯的國際頁面"

#: ../../stattrans.pl:681
msgid "Translated pages (up-to-date)"
msgstr "已翻譯頁面 (最新)"

#: ../../stattrans.pl:688 ../../stattrans.pl:838
msgid "Translated templates (PO files)"
msgstr "已翻譯模板 (PO [CN:文件:][HKTW:檔案:])"

#: ../../stattrans.pl:689 ../../stattrans.pl:841
msgid "PO Translation Statistics"
msgstr "PO 翻譯統計"

#: ../../stattrans.pl:692 ../../stattrans.pl:855
msgid "Fuzzy"
msgstr "模糊"

#: ../../stattrans.pl:693
msgid "Untranslated"
msgstr "未翻譯"

#: ../../stattrans.pl:694
msgid "Total"
msgstr "總數"

#: ../../stattrans.pl:711
msgid "Total:"
msgstr "總數："

#: ../../stattrans.pl:745
msgid "Translated web pages"
msgstr "已翻譯網頁"

#: ../../stattrans.pl:748
msgid "Translation Statistics by Page Count"
msgstr "翻譯統計，按頁面數量"

#: ../../stattrans.pl:763 ../../stattrans.pl:809 ../../stattrans.pl:853
msgid "Language"
msgstr "語言"

#: ../../stattrans.pl:764 ../../stattrans.pl:810
msgid "Translations"
msgstr "翻譯"

#: ../../stattrans.pl:791
msgid "Translated web pages (by size)"
msgstr "已翻譯網頁（按[CN:文件:][HKTW:檔案:]大小）"

#: ../../stattrans.pl:794
msgid "Translation Statistics by Page Size"
msgstr "翻譯統計，按頁面大小"

#~ msgid "Origin"
#~ msgstr "原文"

#~ msgid "Created with"
#~ msgstr "此網頁創建工具："

#~ msgid "Commit diff"
#~ msgstr "提交差异"

#~ msgid "Colored diff"
#~ msgstr "带颜色差异"

#~ msgid "Unified diff"
#~ msgstr "合并差异"
