<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>The following vulnerability has been discovered in the webkit2gtk web
engine:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10018">CVE-2020-10018</a>

   <p>Sudhakar Verma, Ashfaq Ansari and Siddhant Badhe discovered that
   processing maliciously crafted web content may lead to arbitrary
   code execution.</p></li>

</ul>

<p>For the stable distribution (buster), this problem has been fixed in
version 2.26.4-1~deb10u2.</p>

<p>We recommend that you upgrade your webkit2gtk packages.</p>

<p>For the detailed security status of webkit2gtk please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4641.data"
# $Id: $
