<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Multiple security issues have been found in the Mozilla Firefox web
browser, which could potentially result in the execution of arbitrary
code, information disclosure, cross-site scripting or denial of service.</p>

<p>Debian follows the extended support releases (ESR) of Firefox. Support
for the 60.x series has ended, so starting with this update we're now
following the 68.x releases.</p>

<p>For the oldstable distribution (stretch), some additional config changes
to the buildd network are needed (to provide the new Rust-based toolchain
needed by ESR68). Packages will be made available when those are sorted out.</p>

<p>For the stable distribution (buster), these problems have been fixed in
version 68.2.0esr-1~deb10u1.</p>

<p>We recommend that you upgrade your firefox-esr packages.</p>

<p>For the detailed security status of firefox-esr please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/firefox-esr">\
https://security-tracker.debian.org/tracker/firefox-esr</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4549.data"
# $Id: $
