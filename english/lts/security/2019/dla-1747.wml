<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Eli Biham and Lior Neumann discovered a cryptographic weakness in the
Bluetooth LE SC pairing protocol, called the Fixed Coordinate Invalid
Curve Attack (<a href="https://security-tracker.debian.org/tracker/CVE-2018-5383">CVE-2018-5383</a>).  Depending on the devices used, this
could be exploited by a nearby attacker to obtain sensitive
information, for denial of service, or for other security impact.</p>

<p>This flaw has been fixed in firmware for Intel Wireless 7260 (B3),
7260 (B5), 7265 (D1), and 8264 adapters, and for Qualcomm Atheros
QCA61x4 <q>ROME</q> version 3.2 adapters.  Other Bluetooth adapters are
also affected and remain vulnerable.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
20161130-5~deb8u1.</p>

<p>We recommend that you upgrade your firmware-nonfree packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1747.data"
# $Id: $
