<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19318">CVE-2019-19318</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2019-19813">CVE-2019-19813</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2019-19816">CVE-2019-19816</a>

    <p><q>Team bobfuzzer</q> reported bugs in Btrfs that could lead to a
    use-after-free or heap buffer overflow, and could be triggered by
    crafted filesystem images.  A user permitted to mount and access
    arbitrary filesystems could use these to cause a denial of service
    (crash or memory corruption) or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27815">CVE-2020-27815</a>

    <p>A flaw was reported in the JFS filesystem code allowing a local
    attacker with the ability to set extended attributes to cause a
    denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27825">CVE-2020-27825</a>

    <p>Adam <q>pi3</q> Zabrocki reported a use-after-free flaw in the ftrace
    ring buffer resizing logic due to a race condition, which could
    result in denial of service or information leak.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28374">CVE-2020-28374</a>

    <p>David Disseldorp discovered that the LIO SCSI target implementation
    performed insufficient checking in certain XCOPY requests. An
    attacker with access to a LUN and knowledge of Unit Serial Number
    assignments can take advantage of this flaw to read and write to any
    LIO backstore, regardless of the SCSI transport settings.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-29568">CVE-2020-29568</a> (<a href="https://xenbits.xen.org/xsa/advisory-349.html">XSA-349</a>)

    <p>Michael Kurth and Pawel Wieczorkiewicz reported that frontends can
    trigger OOM in backends by updating a watched path.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-29569">CVE-2020-29569</a> (<a href="https://xenbits.xen.org/xsa/advisory-350.html">XSA-350</a>)

    <p>Olivier Benjamin and Pawel Wieczorkiewicz reported a use-after-free
    flaw which can be triggered by a block frontend in Linux blkback. A
    misbehaving guest can trigger a dom0 crash by continuously
    connecting / disconnecting a block frontend.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-29660">CVE-2020-29660</a>

    <p>Jann Horn reported a locking inconsistency issue in the tty
    subsystem which may allow a local attacker to mount a
    read-after-free attack against TIOCGSID.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-29661">CVE-2020-29661</a>

    <p>Jann Horn reported a locking issue in the tty subsystem which can
    result in a use-after-free. A local attacker can take advantage of
    this flaw for memory corruption or privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-36158">CVE-2020-36158</a>

    <p>A buffer overflow flaw was discovered in the mwifiex WiFi driver
    which could result in denial of service or the execution of
    arbitrary code via a long SSID value.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3178">CVE-2021-3178</a>

    <p>吴异 reported an information leak in the NFSv3 server.  When only
    a subdirectory of a filesystem volume is exported, an NFS client
    listing the exported directory would obtain a file handle to the
    parent directory, allowing it to access files that were not meant
    to be exported.</p>

    <p>Even after this update, it is still possible for NFSv3 clients to
    guess valid file handles and access files outside an exported
    subdirectory, unless the <q>subtree_check</q> export option is enabled.
    It is recommended that you do not use that option but only export
    whole filesystem volumes.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3347">CVE-2021-3347</a>

    <p>It was discovered that PI futexes have a kernel stack use-after-free
    during fault handling. An unprivileged user could use this flaw to
    crash the kernel (resulting in denial of service) or for privilege
    escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-26930">CVE-2021-26930</a> (<a href="https://xenbits.xen.org/xsa/advisory-365.html">XSA-365</a>)

    <p>Olivier Benjamin, Norbert Manthey, Martin Mazein, and Jan
    H. Schönherr discovered that the Xen block backend driver
    (xen-blkback) did not handle grant mapping errors correctly.  A
    malicious guest could exploit this bug to cause a denial of
    service (crash), or possibly an information leak or privilege
    escalation, within the domain running the backend, which is
    typically dom0.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-26931">CVE-2021-26931</a> (<a href="https://xenbits.xen.org/xsa/advisory-362.html">XSA-362</a>), <a href="https://security-tracker.debian.org/tracker/CVE-2021-26932">CVE-2021-26932</a> (<a href="https://xenbits.xen.org/xsa/advisory-361.html">XSA-361</a>), <a href="https://security-tracker.debian.org/tracker/CVE-2021-28038">CVE-2021-28038</a> (<a href="https://xenbits.xen.org/xsa/advisory-367.html">XSA-367</a>)

    <p>Jan Beulich discovered that the Xen support code and various Xen
    backend drivers did not handle grant mapping errors correctly.  A
    malicious guest could exploit these bugs to cause a denial of
    service (crash) within the domain running the backend, which is
    typically dom0.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-27363">CVE-2021-27363</a>

    <p>Adam Nichols reported that the iSCSI initiator subsystem did not
    properly restrict access to transport handle attributes in sysfs.
    On a system acting as an iSCSI initiator, this is an information
    leak to local users and makes it easier to exploit <a href="https://security-tracker.debian.org/tracker/CVE-2021-27364">CVE-2021-27364</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-27364">CVE-2021-27364</a>

    <p>Adam Nichols reported that the iSCSI initiator subsystem did not
    properly restrict access to its netlink management interface.  On
    a system acting as an iSCSI initiator, a local user could use
    these to cause a denial of service (disconnection of storage) or
    possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-27365">CVE-2021-27365</a>

    <p>Adam Nichols reported that the iSCSI initiator subsystem did not
    correctly limit the lengths of parameters or <q>passthrough PDUs</q>
    sent through its netlink management interface.  On a system acting
    as an iSCSI initiator, a local user could use these to leak the
    contents of kernel memory, to cause a denial of service (kernel
    memory corruption or crash), and probably for privilege
    escalation.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
4.9.258-1.</p>

<p>We recommend that you upgrade your linux packages.</p>

<p>For the detailed security status of linux please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/linux">https://security-tracker.debian.org/tracker/linux</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2586.data"
# $Id: $
